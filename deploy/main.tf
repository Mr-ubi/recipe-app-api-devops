terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 2.54.0"
    }
  }
  backend "s3" {
    bucket         = "recipe-app-api-devops-tfstatefile"
    key            = "recipe-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}

provider "aws" {
  region  = "us-east-1"
  profile = "adm.hskladowski"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}